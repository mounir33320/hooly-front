import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";
import {Response} from "../../../shared/interfaces/response";
import {FoodTruck} from "../../../shared/interfaces/food-truck";

@Injectable({
  providedIn: 'root'
})
export class FoodTruckService {

  apiUrl = `${environment.api}/api/food-trucks`;

  constructor(private http: HttpClient) { }

  getAll(): Observable<Response>{
    return this.http.get<Response>(`${this.apiUrl}`);
  }

  get(id: number):Observable<Response>{
    return this.http.get<Response>(`${this.apiUrl}/${id}`);
  }


  create(foodTruck: FoodTruck){
    return this.http.post<Response>(`${this.apiUrl}`, foodTruck);
  }

  delete(id: number): Observable<Response>{
    return this.http.delete<Response>(`${this.apiUrl}/${id}`);
  }
}
