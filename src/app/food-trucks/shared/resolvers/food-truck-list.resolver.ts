import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {FoodTruck} from "../../../shared/interfaces/food-truck";
import {Observable, of} from "rxjs";
import {FoodTruckService} from "../services/food-truck.service";
import {switchMap} from "rxjs/operators";

@Injectable({ providedIn: 'root' })
export class FoodTruckListResolver implements Resolve<FoodTruck[]>{
  constructor(private foodTruckService: FoodTruckService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FoodTruck[]> | Promise<FoodTruck[]> | FoodTruck[] {
    return this.foodTruckService.getAll().pipe(
      switchMap(response => {
        return of(response.data)
      })
    )

  }

}
