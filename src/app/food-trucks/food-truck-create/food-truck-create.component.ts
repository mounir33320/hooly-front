import { Component, OnInit } from '@angular/core';
import {FoodTruckService} from "../shared/services/food-truck.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ToastHelper} from "../../shared/helpers/toast.helper";
import {LoaderService} from "../../shared/loader/loader.service";

@Component({
  selector: 'app-food-truck-create',
  templateUrl: './food-truck-create.component.html',
  styleUrls: ['./food-truck-create.component.scss']
})
export class FoodTruckCreateComponent implements OnInit {

  form: FormGroup;

  constructor(
    private foodTruckService: FoodTruckService,
    private router: Router,

    private toastHelper: ToastHelper, private loaderService: LoaderService) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(){
    this.form = new FormGroup({
      name: new FormControl("", [Validators.required])
    })
  }

  onSubmit() {
    this.loaderService.emitLoader(true);
    this.foodTruckService.create(this.form.value).subscribe(
      async (response) => {
        this.toastHelper.display("success", "Food Truck ajouté avec succès !");
        await this.router.navigate(["food-trucks"]);

        this.loaderService.emitLoader(false);
      },
      error =>
      {
        this.loaderService.emitLoader(false);
        this.toastHelper.display("error", "Une erreur est survenue...")
      }

    )
  }
}
