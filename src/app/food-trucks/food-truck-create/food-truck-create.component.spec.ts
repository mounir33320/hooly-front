import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodTruckCreateComponent } from './food-truck-create.component';

describe('FoodTruckCreateComponent', () => {
  let component: FoodTruckCreateComponent;
  let fixture: ComponentFixture<FoodTruckCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodTruckCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodTruckCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
