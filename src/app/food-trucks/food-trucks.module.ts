import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FoodTrucksRoutingModule } from './food-trucks-routing.module';
import { FoodTruckListComponent } from './food-truck-list/food-truck-list.component';
import { FoodTruckCreateComponent } from './food-truck-create/food-truck-create.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {LoaderModule} from "../shared/loader/loader.module";

@NgModule({
  declarations: [
    FoodTruckListComponent,
    FoodTruckCreateComponent
  ],
  imports: [
    CommonModule,
    FoodTrucksRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    LoaderModule
  ]
})
export class FoodTrucksModule { }
