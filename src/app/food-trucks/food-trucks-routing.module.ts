import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FoodTruckListComponent} from "./food-truck-list/food-truck-list.component";
import {FoodTruckCreateComponent} from "./food-truck-create/food-truck-create.component";
import {FoodTruckListResolver} from "./shared/resolvers/food-truck-list.resolver";

const routes: Routes = [
  {
    path: "",
    resolve: {
      foodTruckList: FoodTruckListResolver
    },
    component: FoodTruckListComponent
  },
  { path: "create", component: FoodTruckCreateComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FoodTrucksRoutingModule { }
