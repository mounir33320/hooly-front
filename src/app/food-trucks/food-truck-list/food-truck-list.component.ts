import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FoodTruck} from "../../shared/interfaces/food-truck";
import {FoodTruckService} from "../shared/services/food-truck.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator, MatPaginatorIntl} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {ActivatedRoute} from "@angular/router";
import {delay, map, switchMap} from "rxjs/operators";
import {of, Subscription} from "rxjs";
import {ToastHelper} from "../../shared/helpers/toast.helper";
import {LoaderService} from "../../shared/loader/loader.service";

export function customPaginator(){
  const customPaginator = new MatPaginatorIntl();
  customPaginator.itemsPerPageLabel = "Élements par page";
  customPaginator.nextPageLabel = "Page suivate";
  customPaginator.previousPageLabel = "Page précédente";
  return customPaginator;
}

@Component({
  selector: 'app-food-truck-list',
  templateUrl: './food-truck-list.component.html',
  styleUrls: ['./food-truck-list.component.scss'],
  providers: [
    { provide: MatPaginatorIntl, useValue: customPaginator() }
  ]
})
export class FoodTruckListComponent implements OnInit  {
  displayedColumns: string[] = ['name', 'action'];
  dataSource: MatTableDataSource<FoodTruck>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  foodTruckList: FoodTruck[];

  constructor(
    private foodTruckService: FoodTruckService,
    private route: ActivatedRoute,
    private toastHelper: ToastHelper,
    private loaderService: LoaderService
  ) { }

  ngOnInit(): void {
    this.route.data.pipe(
      switchMap(data => {
        this.foodTruckList = data.foodTruckList;
        this.dataSource = new MatTableDataSource(this.foodTruckList)
        return of(this.dataSource)
      }),
      delay(0)
    ).subscribe(dataSource => {
        dataSource.paginator = this.paginator;
        dataSource.sort = this.sort;
    })
  }

  onDelete(id: number) {
    this.loaderService.emitLoader(true);

    this.foodTruckService.delete(id).subscribe(
      () => {
        this.foodTruckList = this.foodTruckList.filter(element => {
          return element.id !== id
        })
        this.dataSource.data = this.foodTruckList;
        this.toastHelper.display("success", "Food Truck supprimé avec succès")
        this.loaderService.emitLoader(false);
      }
    )
  }
}
