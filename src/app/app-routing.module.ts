import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "reservations",
    loadChildren: () => import("./reservations/reservations.module").then(m => m.ReservationsModule)
  },
  {
    path: "food-trucks",
    loadChildren: () => import("./food-trucks/food-trucks.module").then(m => m.FoodTrucksModule)
  },
  {
    path: "",
    redirectTo: "reservations",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
