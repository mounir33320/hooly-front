import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  loaderSubject = new BehaviorSubject(false);
  constructor() { }

  emitLoader(isLoading: boolean){
    this.loaderSubject.next(isLoading);
  }
}
