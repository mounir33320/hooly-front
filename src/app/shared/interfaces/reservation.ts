import {FoodTruck} from "./food-truck";

export interface Reservation {
  id: number,
  bookedOn: string,
  foodTruck: FoodTruck
}
