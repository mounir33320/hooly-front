import {Injectable} from "@angular/core";
import {Notyf} from "notyf";

@Injectable({ providedIn: 'root' })
export class ToastHelper{

  notyf = new Notyf()

  display(type: string, message: string){
    this.notyf.open({
      type: type,
      message: message,
      duration: 3000,
      position: {
        x: "right",
        y: "top"
      }
    })
  }
}
