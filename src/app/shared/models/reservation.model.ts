import {Reservation} from "../interfaces/reservation";
import {FoodTruck} from "../interfaces/food-truck";

export class ReservationModel implements Reservation{
  id: number;
  bookedOn: string;
  foodTruck: FoodTruck;
}
