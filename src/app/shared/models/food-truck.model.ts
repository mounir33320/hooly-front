import {FoodTruck} from "../interfaces/food-truck";

export class FoodTruckModel implements FoodTruck{
  id: number;
  name: string;

  constructor(id: number, name: string = "") {
    this.id = id;
    this.name =  name;
  }
}
