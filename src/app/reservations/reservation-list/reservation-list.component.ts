import {Component, OnInit, ViewChild} from '@angular/core';
import {ReservationService} from "../shared/services/reservation.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Reservation} from "../../shared/interfaces/reservation";
import {ActivatedRoute} from "@angular/router";
import {delay, switchMap} from "rxjs/operators";
import {of} from "rxjs";
import {ToastHelper} from "../../shared/helpers/toast.helper";
import {LoaderService} from "../../shared/loader/loader.service";

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnInit {
  displayedColumns: string[] = ['bookedOn', 'foodTruck', 'action'];
  dataSource: MatTableDataSource<Reservation>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  reservationList: Reservation[];

  constructor(
    private reservationService: ReservationService,
    private route: ActivatedRoute,
    private toastHelper: ToastHelper,
    private loaderService: LoaderService
  ) { }

  ngOnInit(): void {
    this.route.data.pipe(
      switchMap(data => {
        this.reservationList = data.reservationList;
        this.dataSource = new MatTableDataSource(this.reservationList)
        return of(this.dataSource)
      }),
      delay(0)
    ).subscribe(dataSource => {
      dataSource.paginator = this.paginator;
      setTimeout(() => {
        dataSource.sort = this.sort;
      },3000)
    })
  }

  onDelete(id: number) {
    this.loaderService.emitLoader(true);

    this.reservationService.delete(id).subscribe(
      () => {
        this.reservationList = this.reservationList.filter(element => {
          return element.id !== id
        })
        this.dataSource.data = this.reservationList;
        this.toastHelper.display("success", "Réservation supprimée avec succès")
        this.loaderService.emitLoader(false);

      },
      error => {
        this.toastHelper.display("error", "Une erreur est survenue...")
        this.loaderService.emitLoader(false);
      }
    )
  }
}
