import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReservationListComponent} from "./reservation-list/reservation-list.component";
import {ReservationCreateComponent} from "./reservation-create/reservation-create.component";
import {ReservationListResolver} from "./shared/resolvers/reservation-list.resolver";
import {FoodTruckListResolver} from "../food-trucks/shared/resolvers/food-truck-list.resolver";

const routes: Routes = [
  {
    path: "",
    component: ReservationListComponent,
    resolve: {
      reservationList: ReservationListResolver
    }
  },
  {
    path: "create",
    component: ReservationCreateComponent,
    resolve: {
      foodTruckList: FoodTruckListResolver
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservationsRoutingModule { }
