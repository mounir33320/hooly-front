import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Response} from "../../../shared/interfaces/response";
import {Reservation} from "../../../shared/interfaces/reservation";

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  apiUrl = `${environment.api}/api/reservations`;

  constructor(private http: HttpClient) { }

  getAll():Observable<Response>{
    return this.http.get<Response>(`${this.apiUrl}`);
  }

  create(reservation: Reservation):Observable<Response>{
    return this.http.post<Response>(`${this.apiUrl}`, reservation);
  }

  delete(id: number): Observable<Response>{
    return this.http.delete<Response>(`${this.apiUrl}/${id}`);
  }
}
