import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Observable, of} from "rxjs";
import {switchMap} from "rxjs/operators";
import {ReservationService} from "../services/reservation.service";
import {Reservation} from "../../../shared/interfaces/reservation";

@Injectable({ providedIn: 'root' })
export class ReservationListResolver implements Resolve<Reservation[]>{
  constructor(private reservationService: ReservationService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Reservation[]> | Promise<Reservation[]> | Reservation[] {
    return this.reservationService.getAll().pipe(
      switchMap(response => {
        return of(response.data)
      })
    )

  }

}
