import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {FoodTruck} from "../../shared/interfaces/food-truck";
import {ReservationService} from "../shared/services/reservation.service";
import {FoodTruckService} from "../../food-trucks/shared/services/food-truck.service";
import {ToastHelper} from "../../shared/helpers/toast.helper";
import {ActivatedRoute, Router} from "@angular/router";
import {ReservationModel} from "../../shared/models/reservation.model";
import {FoodTruckModel} from "../../shared/models/food-truck.model";
import {MatDatepickerInputEvent} from "@angular/material/datepicker";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from "@angular/material/core";
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from "@angular/material-moment-adapter";
import {LoaderService} from "../../shared/loader/loader.service";

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

@Component({
  selector: 'app-reservation-create',
  templateUrl: './reservation-create.component.html',
  styleUrls: ['./reservation-create.component.scss'],
  providers: [
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    }
  ]
})
export class ReservationCreateComponent implements OnInit {

  form: FormGroup;
  foodTruckList: FoodTruck[];

  myFilter = (d: Date | null ): boolean => {
    // @ts-ignore
    return d > new Date();
  };

  constructor(
    private reservationService: ReservationService,
    private foodTruckService: FoodTruckService,
    private toastHelper: ToastHelper,
    private route: ActivatedRoute,
    private router: Router,
    private loaderService: LoaderService
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => this.foodTruckList = data.foodTruckList);
    this.initForm();
  }

  initForm(){
    this.form = new FormGroup({
      bookedOn: new FormControl(""),
      foodTruck: new FormControl("", [Validators.required])
    })
  }

  onSubmit() {
    this.loaderService.emitLoader(true);

    const reservation = new ReservationModel();
    reservation.bookedOn = this.formatDate(this.form.value.bookedOn);
    reservation.foodTruck = new FoodTruckModel(this.form.value.foodTruck);

    this.reservationService.create(reservation).subscribe(
      async response => {
        this.toastHelper.display("success", "Réservation créée avec succès");
        await this.router.navigate(["reservations"])

        this.loaderService.emitLoader(false);
      },
      error => {
        this.loaderService.emitLoader(false);

        this.toastHelper.display("error", error.error.message)
      }
    )
  }

  private formatDate(date: Date): string{
    date = new Date(date.toString())
    const month = date.getMonth().toString().length === 1 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
    const day = date.getDate().toString().length === 1 ? `0${date.getDate()}` : date.getDate();
    const year = date.getFullYear();

    return `${year}-${month}-${day}`;
  }
}
