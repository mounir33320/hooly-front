import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoaderService} from "./shared/loader/loader.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  isLoading = false;
  loaderSubscription: Subscription;

  constructor(private loaderService: LoaderService) {
  }

  ngOnInit(): void {
    this.loaderSubscription = this.loaderService.loaderSubject.subscribe(
      isLoading => this.isLoading = isLoading
    )
  }

  ngOnDestroy(): void {
    this.loaderSubscription.unsubscribe();
  }
}
